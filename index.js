import Specdom  from 'specdom';
import Actions from './Actions';
import Reducer from './Reducer';
//import clone from './clone';

var global = window || global;

export default function(init_state, custom_reducers, cb){

  var reducer = Reducer(custom_reducers);

  var create_store = require('redux').createStore;
  var store = create_store(reducer, init_state);
  var actions = Actions(store, custom_reducers);

  /** anonymous function that runs when the store is updated. */
  store.subscribe(function(){
    var state = store.getState();
    cb(state, actions);
  });

  return actions;
}
